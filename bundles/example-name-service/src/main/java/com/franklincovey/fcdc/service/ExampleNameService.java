package com.franklincovey.fcdc.service;

public interface ExampleNameService {

    public String[] getNames();
    public void setNames(String[] names);
    
}
